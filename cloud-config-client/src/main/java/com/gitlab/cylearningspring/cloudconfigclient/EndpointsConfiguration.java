package com.gitlab.cylearningspring.cloudconfigclient;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


@Configuration
@RefreshScope
public class EndpointsConfiguration
{
    @Value("${new.greeting.service.enabled:false}")
    private boolean newGreetingServiceEnabled;

    private final SuperGreetingService superGreetingService;

    private final PlainGreetingService plainGreetingService;


    @Autowired
    public EndpointsConfiguration(
            final SuperGreetingService superGreetingService,
            final PlainGreetingService plainGreetingService)
    {
        this.superGreetingService = superGreetingService;
        this.plainGreetingService = plainGreetingService;
    }


    @Bean
    @Primary
    @RefreshScope
    public GreetingService greetingService()
    {
        return newGreetingServiceEnabled ? superGreetingService : plainGreetingService;
    }
}
