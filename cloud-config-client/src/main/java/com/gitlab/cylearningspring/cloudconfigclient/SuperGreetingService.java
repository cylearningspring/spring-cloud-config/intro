package com.gitlab.cylearningspring.cloudconfigclient;


import org.springframework.stereotype.Service;


@Service
public class SuperGreetingService implements GreetingService
{
    @Override
    public String greet(final String name, final String role)
    {
        return String.format("Hello %s!%nYou will become an amazing %s...", name, role);
    }
}
