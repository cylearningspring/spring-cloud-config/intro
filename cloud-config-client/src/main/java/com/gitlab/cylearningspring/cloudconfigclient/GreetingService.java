package com.gitlab.cylearningspring.cloudconfigclient;


public interface GreetingService
{
    String greet(String name, String role);
}
