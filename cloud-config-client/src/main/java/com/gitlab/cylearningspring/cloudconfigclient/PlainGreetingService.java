package com.gitlab.cylearningspring.cloudconfigclient;


import org.springframework.stereotype.Service;


@Service
public class PlainGreetingService implements GreetingService
{
    @Override
    public String greet(final String name, final String role)
    {
        return String.format("Hello!%nYou are %s and you will become a(n) %s...", name, role);
    }
}
