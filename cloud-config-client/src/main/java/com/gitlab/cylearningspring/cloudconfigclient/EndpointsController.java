package com.gitlab.cylearningspring.cloudconfigclient;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RefreshScope
@RestController
public class EndpointsController
{
    @Value("${user.role:N/A}")
    private String role;

    private final GreetingService greetingService;


    @Autowired
    public EndpointsController(final GreetingService greetingService)
    {
        this.greetingService = greetingService;
    }


    @GetMapping(value = "/whoami/{username}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String whoami(@PathVariable String username) {
        return greetingService.greet(username, role);
    }
}
